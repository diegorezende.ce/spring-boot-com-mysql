package br.com.diegorezende.springbootcommysql.serviceTests;
import static org.assertj.core.api.Assertions.assertThat;

import br.com.diegorezende.springbootcommysql.controller.dto.PessoaRq;
import br.com.diegorezende.springbootcommysql.controller.dto.PessoaRs;
import br.com.diegorezende.springbootcommysql.model.Pessoa;
import br.com.diegorezende.springbootcommysql.repository.PessoaRepository;
import br.com.diegorezende.springbootcommysql.service.PessoaService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;


import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PessoaServiceTests {


    @Mock
    private PessoaRepository pessoaRepository;
    @InjectMocks
    private PessoaService pessoaService;

    @BeforeEach
    public void setUp() {
        openMocks(this);
        pessoaService = new PessoaService(pessoaRepository);
    }


    @Test
    @DisplayName("findAll() -> Deveria listar todos as pessoas presentes no Database")
    public void findAll() {
        //Cenario - Preparacao do Mock (Arrange)
        List<Pessoa> pessoasNoDatabaseMock = new ArrayList<>();
        Pessoa diego = new Pessoa(1L,"Diego","Rezende");
        Pessoa eva = new Pessoa(2L,"Eva","Nikoli");
        Pessoa lePrince = new Pessoa(3L, "Luis","Le Prince");
        pessoasNoDatabaseMock.add(diego);
        pessoasNoDatabaseMock.add(eva);
        pessoasNoDatabaseMock.add(lePrince);

        // Set up the mock
        when(pessoaRepository.findAll()).thenReturn(pessoasNoDatabaseMock);


        //Teste/execucao - Chamada ao Servico (Act)
        List<PessoaRs> resultadoFindAll = pessoaService.findAll();


        //Verificacao - (Assert)
        assertThat(resultadoFindAll).isNotNull().hasSize(3);
        assertThat(resultadoFindAll.get(0).getId()).isEqualTo(1L);
        assertThat(resultadoFindAll.get(0).getNome()).isEqualTo("Diego");
        assertThat(resultadoFindAll.get(0).getSobrenome()).isEqualTo("Rezende");
        assertThat(resultadoFindAll.get(1).getId()).isEqualTo(2L);
        assertThat(resultadoFindAll.get(1).getNome()).isEqualTo("Eva");
        assertThat(resultadoFindAll.get(1).getSobrenome()).isEqualTo("Nikoli");
        assertThat(resultadoFindAll.get(2).getId()).isEqualTo(3L);
        assertThat(resultadoFindAll.get(2).getNome()).isEqualTo("Luis");
        assertThat(resultadoFindAll.get(2).getSobrenome()).isEqualTo("Le Prince");
        verify(pessoaRepository, times(1)).findAll();

    }

    @Test
    @DisplayName("findById() -> Deveria listar uma pessoa presente no Database pelo Id ")
    public void findById() throws Exception {
        //Cenario
        Pessoa diego = new Pessoa(1L,"Diego","Rezende");

        //Set Up the Mock
        when(pessoaRepository.getReferenceById(1L)).thenReturn(diego);

        //Teste
        PessoaRs resultadoFindById = pessoaService.findById(1L);

        //Verifica
        assertThat(resultadoFindById.getId()).isEqualTo(1L);
        assertThat(resultadoFindById.getNome()).isEqualTo("Diego");
        assertThat(resultadoFindById.getSobrenome()).isEqualTo("Rezende");
        verify(pessoaRepository, times(1)).getReferenceById(1L);


    }

    @Test
    @DisplayName("savePerson() -> Deveria adicionar uma pessoa no Database")
    public void savePerson() {
        //Cenario

        PessoaRq diegoRq = new PessoaRq();
        diegoRq.setNome("Diego");
        diegoRq.setSobrenome("Rezende");

        Pessoa diego = new Pessoa();
        diego.setId(1L);
        diego.setNome("Diego");
        diego.setSobrenome("Rezende");

        //Set up the Mock
        when(pessoaRepository.save(diego)).thenReturn(diego);

        //Teste
        pessoaService.savePerson(diegoRq);

        //Verifica se o metodo save foi chamado
        verify(pessoaRepository, times(1)).save(any());


    }

    @Test
    @DisplayName("updatePerson() -> Deveria atualizar uma pessoa presente no Database através do Id")
    public void updatePerson() {
        //Cenario

        //Mock

        //Test

        //Verificação

    }

    @Test
    @DisplayName("deletePerson() -> Deveria apagar uma pessoa presente no Database através do Id")
    public void deletePerson() {
        //Cenario

        //Mock

        //Test

        //Verificação
    }


}