package br.com.diegorezende.springbootcommysql.controller.dto;



public class PessoaRq {
    private String nome;
    private String sobrenome;

    public PessoaRq() {
    }
    public PessoaRq(String nome, String sobrenome) {
        this.setNome(nome);
        this.setSobrenome(sobrenome);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
}
